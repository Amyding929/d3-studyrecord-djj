/*笔记*/


//1.轴
//define x axis
var xAxis = d3.svg.axis()
	.scale(xScale)
	.orient("bottom")//y axis orient("left")
	.ticks(5);//set rough of ticks

//create x axis
    svg.append("g")
    	.call(xAxis);

//调整坐标位置到下面：
	svg.append("g")
		.attr("class","axis")
		.attr("transform","translate(0," + ( h- padding) + ")")
		.call(xAxis);

//设置刻度的格式
var formatAsPercentage = d3.format(".1%");
	xAxis.tickFormat(formatAsPercentage);

//css
.axis path,
.axis line{
	fill:none;
	stroke:black;
	shape-rendering:crispEdges;
}
.axis text{
	font-family:sans-serif;
	font-size:11px;
}